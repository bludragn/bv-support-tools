package getplayerinfo;

import au.com.bytecode.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class GetPlayerInfo {
 
	// http://localhost:8080/RESTfulExample/json/product/get
	public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
            
          String end = "hello";  
          Pattern endPattern = Pattern.compile("data\":\\[\\]");
          
          /*
          String in = "Let's match 127.0.0.1 being valid, or this IP: 127.0.0.1 and joinTHIS.server.com or build 1.2";
        String re = "\\b((?:(?:[0-9]{1,3}\\.){3}[0-9]{1,3}|(?:[a-z0-9]+(?:-[a-z0-9]+)*\\.)+[a-z]{2,4}))\\b";
        Pattern p = Pattern.compile(re, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(in);
        while (m.find()) {
          
           */
            
          // while (matcher.find()) { }
          
          try {
 
		// example string - String content = "This is the content to write into file";
 
			File file = new File("/Users/chrisnellis/Desktop/GetBVInfo/dist/output.txt");
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
                        
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
 
          CSVReader labelReader = new CSVReader(new FileReader("/Users/chrisnellis/Desktop/GetBVInfo/dist/labellist.csv"));
            String [] nextLabelLine;
                        
          CSVReader reader = new CSVReader(new FileReader("/Users/chrisnellis/Desktop/GetBVInfo/dist/list.csv"));
            String [] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                
                Thread.sleep(1500);
                
                try {
 
		URL url = new URL(nextLine[0]);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
 
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}
 
		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));
 
		String output;
/*                String labelQualifier = "Site: ";
                String labelValue = Arrays.toString(labelReader.readNext());
                String labelComplete = labelQualifier + labelValue;
                */
		//System.out.println("Site: " + labelValue);
		while ((output = br.readLine()) != null) {
			
                    Matcher endOfData = endPattern.matcher(output);
                    
                    if (!endOfData.find()) {
                    //System.out.println(output);
                        
                       //Matcher matcher = pattern.matcher(output);
                       //if (matcher.find()) {
                           //String result = matcher.replaceAll("\n{\"_id\":");
                        
                        
                        //if (!endOfData.find())  //endOfData.find()
                        //{
                            
                        //String result = output.replace("{\"_id", "\n{\"_id"); //--this is for activities data OR player data
                        String result = output.replace("{\"type", "\n{\"type"); //--this is for reward definitions
                        //String result = output.replace("{\"display", "\n{\"display"); //--this was for leaderboard rankings data
                            
                            
                        //bw.write(labelComplete + "\n");
                        bw.write(result + "\n\n");
                    
                        //System.out.println(labelComplete + "\n");
                        System.out.println(result + "\n\n");
                        //System.out.println(end);
                    }
                        
                        else
                        {
                        System.out.println("End of Data Found" + "\n");
                    }
                        
                     
                    }
		
 
		conn.disconnect();
 
	  } catch (MalformedURLException e) {
 
		e.printStackTrace();
 
	  } catch (IOException e) {
 
		e.printStackTrace();
 
	  }
                
                
            // nextLine[] is an array of values from the line
           // System.out.println(nextLine[0]);
           // }  
            
            
          /*
          CSVReader reader = new CSVReader(new FileReader("/Users/chrisnellis/Desktop/GetPlayerInfo/dist/list.csv"));
          List myEntries = reader.readAll();
          
          int i = 0;
	  while (i < myEntries.size()) {
		System.out.println(myEntries.get(i));
		i++;
          }
          */
            
	  
 
	}
            bw.close();
            } catch (IOException e) {
			e.printStackTrace();
            // close writer
        }
        }
 
}